<?php

namespace App\Entity;

use App\Repository\PieceRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PieceRepository::class)
 */
class Piece
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nomPiece;

    /**
     * @ORM\Column(type="integer")
     */
    private $quantite;

    /**
     * @ORM\Column(type="float")
     */
    private $unite;
    
     /**
     * @ORM\Column(type="string", length=255)
     */
    private $fournisseur;

    /**
     * @ORM\ManyToOne(targetEntity=Montage::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $Montage;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomPiece(): ?string
    {
        return $this->nomPiece;
    }

    public function setNomPiece(string $nomPiece): self
    {
        $this->nomPiece = $nomPiece;

        return $this;
    }

    public function getQuantite(): ?int
    {
        return $this->quantite;
    }

    public function setQuantite(string $quantite): self
    {
        $this->quantite = $quantite;

        return $this;
    }

    public function getUnite(): ?float
    {
        return $this->unite;
    }

    public function setUnite($unite):self
    {
        $this->unite = $unite;

        return $this;
    }
    
    public function getFournisseur(): ?string
    {
        return $this->fournisseur;
    }
   

    public function setFournisseur(?string $fournisseur): self
    {
        $this->fournisseur = $fournisseur;

        return $this;
    }

    public function getMontage(): ?Montage
    {
        return $this->Montage;
    }

    public function setMontage(?Montage $Montage): self
    {
        $this->Montage = $Montage;

        return $this;
    }
}
