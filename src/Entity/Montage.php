<?php

namespace App\Entity;


use App\Repository\MontageRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\DBAL\Types\DecimalType;
use Symfony\Component\Validator\Constraints as Assert;
use DateTimeInterface;

/**
 * @ORM\Entity(repositoryClass=MontageRepository::class)
 */
class Montage
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nomMontage;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $client;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $createdAt;
    
     /**
     * @ORM\Column(type="float")
     */
    private $cout;

    
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomMontage(): ?string
    {
        return $this->nomMontage;
    }

    public function setNomMontage(string $nomMontage): self
    {
        $this->nomMontage = $nomMontage;

        return $this;
    }

    public function getClient(): ?string
    {
        return $this->client;
    }

    public function setClient(string $client): self
    {
        $this->client = $client;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
      
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getCout(): ?float
    {
        
        return $this->cout;
    }
   

    public function setCout(?float $cout): self
    {
        $this->cout = $cout;

        return $this;
    }

  
}
